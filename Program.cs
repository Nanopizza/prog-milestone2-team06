﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team06
{
    class Program
    {
        static void Main(string[] args)
        {
        the_beginning:
            int loop_cont = 1;
            do
            {
                Console.Clear();
                Console.WriteLine("\n\t\t  HELLO & WELCOME !!!\n");
                Console.WriteLine("\t             OOOOOOOOOOO");
                Console.WriteLine("\t        OOOOOOOOOOOOOOOOOOO");
                Console.WriteLine("\t      OOOOOO  OOOOOOOOO  OOOOOO");
                Console.WriteLine("\t    OOOOOO      OOOOO      OOOOOO");
                Console.WriteLine("\t  OOOOOOOO  #   OOOOO  #   OOOOOOOO");
                Console.WriteLine("\t OOOOOOOOOO    OOOOOOO    OOOOOOOOOO");
                Console.WriteLine("\tOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
                Console.WriteLine("\tOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
                Console.WriteLine("\tOOOO  OOOOOOOOOOOOOOOOOOOOOOOOO  OOOO");
                Console.WriteLine("\t OOOO  OOOOOOOOOOOOOOOOOOOOOOO  OOOO");
                Console.WriteLine("\t  OOOO   OOOOOOOOOOOOOOOOOOOO  OOOO");
                Console.WriteLine("\t   OOOOO   OOOOOOOOOOOOOOO   OOOO");
                Console.WriteLine("\t     OOOOOO   OOOOOOOOO   OOOOOO");
                Console.WriteLine("\t       OOOOOO         OOOOOO");
                Console.WriteLine("\t            OOOOOOOOOOOO\n");
                Console.WriteLine("\t >>>  PRESS ANY KEY TO CONTINUE  <<<");
                Console.WriteLine("\n\n\t\u00a9 Johnny, Finn, Robert, Gurkiert");
                Console.ReadKey();

                Console.Clear();
                Console.WriteLine("\t***************************************************");
                Console.WriteLine("\tPlease choose a number and press the <ENTER> key.\n");
                Console.WriteLine("\t***************************************************\n\n");

                Console.WriteLine("\t1.\tDate Calculator\n");
                Console.WriteLine("\t2.\tGrade Average Calculator\n");
                Console.WriteLine("\t3.\tPlay the Random Number Game\n");
                Console.WriteLine("\t4.\tRate your faviroute food!!\n");
                Console.WriteLine("\t5.\texit the program\n");


                Console.WriteLine("Your Choice: ");
                var choice = 0;
                var a = Console.ReadLine();
                var value_a = int.TryParse(a, out choice);

                if (value_a == true)
                {
                    switch (choice)
                    {
                        case 1:   //Date Calculator-------> (Johnny)                      
                        date_start:
                            int date_opt = 1;

                            do
                            {
                                Console.Clear();
                                Console.WriteLine("\t******************************\n");
                                Console.WriteLine("\tWelcome do the Date calculator\n");
                                Console.WriteLine("\t******************************\n\n");
                                Console.WriteLine("\t-- Press any key to continue --");
                                Console.ReadKey();
                                Console.Clear();
                                Console.WriteLine("\n\n  **********************************************************************");
                                Console.WriteLine("\n\n  Choose a number from the options below and press <ENTER>:\n\n");
                                Console.WriteLine("\t1.   Calculate how many days old you by years.\n");
                                Console.WriteLine("\t2.   Calculate how many days old you are using your DOB.\n ");
                                Console.WriteLine("\t3.   Exit to main menu. ");
                                Console.WriteLine("\n\n  **********************************************************************");

                                var date_choice = 0;
                                var b = Console.ReadLine();
                                var value_b = int.TryParse(b, out date_choice);

                                if (value_b == true)
                                {
                                    if (date_choice == 1)
                                    {
                                        date_calculator2();
                                        date_choice = 3;
                                    }
                                    else if (date_choice == 2)
                                    {
                                        date_calculator();
                                    }
                                    else if (date_choice >= 3)
                                    {
                                        goto the_beginning;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("You entered an incorrect value! try again.");
                                    Console.WriteLine("\n\t--press anykey to continue--");
                                    Console.ReadKey();
                                }

                                Console.WriteLine("\n\tType 1 to enter another date calculation or 2 to exit to the main menu\n\tand then press <ENTER>");
                                var c = 0;
                                var input = Console.ReadLine();
                                var output = int.TryParse(input, out c);

                                if (output == true)
                                {
                                    if (c == 1)
                                    {
                                        goto date_start;
                                    }
                                    else
                                    {
                                        goto the_beginning;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("You entered invalid value...try again");
                                }

                            } while (date_opt == 1);
                            break;

                        case 2://Calculate Grade Average to in case 2 (Finn)
                            Console.Clear();
                            Console.WriteLine("Task 2 content here");
                            Console.ReadLine();
                            break;

                        case 3://Generate a Random Number to go in case 3 (Gurkie)
                            Console.Clear();
                            Console.WriteLine("Task 3 Crap here");
                            Console.ReadLine();
                            break;

                        case 4://Rate your Favourite food to go in case 4 (Robert)
                            int food_opt = 4;
                            Console.Clear();

                            do
                            {
                                FoodRaterAnchor:
                                Food_Rater();
                                Console.WriteLine("\n\tChoose 1 to re-use the food rating program or 2 to return to the main menu:  ");                               
                                food_opt = int.Parse(Console.ReadLine());
                                if (food_opt == 1)
                                {
                                    goto FoodRaterAnchor;
                                }
                                {
                                    goto the_beginning;
                                }
                            } while (food_opt == 4); 
                            break;

                        case 5://case to exit program                                                     
                            loop_cont = 0;
                            break;

                        default:
                            break;
                    }
                }
                else if (value_a == false)
                {
                    Console.WriteLine("You did something wrong! try again!");
                    Console.WriteLine("---Press any key to contintue---");
                    Console.ReadKey();
                    Console.Clear();
                }

            } while (loop_cont == 1);
            Console.Clear();
            Console.WriteLine("\n\n\n\t** Thanks for using our program **");
            Console.WriteLine("\n\t **GoodBye**\n\n\n");
            Console.WriteLine("\n\n\n\n\t\u00a9 Johnny, Finn, Robert, Gurkiert\n\n\n");
        }

        //*********************************  put all your methods down here  **********************************************//

        static void date_calculator() // method for case 1 (Johnny)
        {
            Console.Clear();
            var todays_date = DateTime.Now;

            Console.WriteLine("\n\n");
            Console.WriteLine("\tPlease enter your date of birth eg:  dd/mm/yyyy \n");

            var entry = (Console.ReadLine());
            DateTime dob;
            var value = DateTime.TryParse(entry, out dob);

            if (value == true)
            {
                var years = todays_date - dob;
                Console.WriteLine($"Congratulations you are {years.Days} days old\n");
            }
            else if (value == false)
            {
                Console.WriteLine("  Oops, something went wrong.\n  Please enter your dob again and make sure you are using format dd/mm/yyyy");
                Console.WriteLine("\n\n  ---  Press any key to continue  ---");
                Console.ReadKey();
            }
        }
        static void date_calculator2()  //2nd method for date calculator, case 1 (Johnny)
        {
            Console.Clear();
            Console.WriteLine("\n\n\tPlease enter the number of years you'd like to calculate,\n\tthen press <ENTER>:  ");
            var number = 0;
            var a = Console.ReadLine();
            var input = int.TryParse(a, out number);

            if (input == true)
            {
                if (number >= 4)
                {
                    Console.Clear();
                    var leap_days = number / 4;
                    var days_old = number * 365 - leap_days;
                    Console.WriteLine($"\n\n\t{number} Years makes you {days_old} days old\n");
                }
                else if (number < 4)
                {
                    Console.Clear();
                    var days_old = number * 365;
                    Console.WriteLine($"\n\n\tBased on {number} years that would make you {days_old} days old");
                }
            }
            else
            {
                Console.WriteLine("\t\nYou didnt enter a valid number, try again");
            }
         }
        //___________________________________Start of Roberts__________________________________________
        static void Food_Rater() //method for case 4 (Robert)
        {

                Console.WriteLine("\t*************************\n");
                Console.WriteLine("\tWelcome to the food rater\n");
                Console.WriteLine("\t*************************\n\n");
                Console.WriteLine("\t**** Press any key to continue ****");
                Console.ReadKey();
                Console.WriteLine(" "); //break

                Console.WriteLine("In this program you will be asked to enter your 5 favourite foods and then rate them. Please press Enter to continue");
                Console.ReadLine();

                ratefoodmakedictionary();

                Console.WriteLine("Would you like to re-rate any of your foods? Please type yes or no");

                var reinput = Console.ReadLine();

                switch (reinput)
                {
                    case "yes":
                        ratefoodmakedictionary(); //method 2 for case 4 (Robert)
                        break;
                    case "no":
                        break;
                    default:
                        Console.WriteLine("Invalid input");
                        break;

                }

                Console.WriteLine(" "); //Break
                Console.WriteLine("Thankyou for using this program, press any key to exit");
                Console.ReadLine();
            }


        public static void ratefoodmakedictionary() 
        {
            Console.WriteLine("Please input your 5 favourite foods (1 line per entry)");
            Console.WriteLine(" "); //Break

            Console.WriteLine("Please Input your first food choice");
            var food1 = Console.ReadLine();
            Console.WriteLine("Please Input your second food choice");
            var food2 = Console.ReadLine();
            Console.WriteLine("Please Input your third food choice");
            var food3 = Console.ReadLine();
            Console.WriteLine("Please Input your fourth food choice");
            var food4 = Console.ReadLine();
            Console.WriteLine("Please Input your fifth food choice");
            var food5 = Console.ReadLine();
            Console.WriteLine(" "); //Break

            Console.WriteLine($"Please rate {food1} on a scale of 1 to 5");
            int ratefood1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Please rate {food2} on a scale of 1 to 5");
            int ratefood2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Please rate {food3} on a scale of 1 to 5");
            int ratefood3 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Please rate {food4} on a scale of 1 to 5");
            int ratefood4 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Please rate {food5} on a scale of 1 to 5");
            int ratefood5 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" "); //Break

            var favfoods = new Dictionary<string, string>();
            favfoods.Add($"{food1}", $"{ratefood1}");
            favfoods.Add($"{food2}", $"{ratefood2}");
            favfoods.Add($"{food3}", $"{ratefood3}");
            favfoods.Add($"{food4}", $"{ratefood4}");
            favfoods.Add($"{food5}", $"{ratefood5}");

            if (food1 == string.Empty)
            {
                Console.WriteLine($"You failed to input one of your favourite foods. Please restart the program and try again"); //take back to start 
                Console.WriteLine("Press any key to close the program");
                Console.ReadKey();
                return;
            }
            else
            {
                //do nothing    
            }
            if (food2 == string.Empty)
            {
                Console.WriteLine($"You failed to input one of your favourite foods. Please restart the program and try again"); //take back to start 
                Console.WriteLine("Press any key to close the program");
                Console.ReadKey();
                return;
            }
            else
            {
                //do nothing    
            }
            if (food3 == string.Empty)
            {
                Console.WriteLine($"You failed to input one of your favourite foods. Please restart the program and try again"); //take back to start 
                Console.WriteLine("Press any key to close the program");
                Console.ReadKey();
                return;
            }
            else
            {
                //do nothing    
            }
            if (food4 == string.Empty)
            {
                Console.WriteLine($"You failed to input one of your favourite foods. Please restart the program and try again"); //take back to start 
                Console.WriteLine("Press any key to close the program");
                Console.ReadKey();
                return;
            }
            else
            {
                //do nothing    
            }
            if (food5 == string.Empty)
            {
                Console.WriteLine($"You failed to input one of your favourite foods. Please restart the program and try again"); //take back to start 
                Console.WriteLine("Press any key to close the program");
                Console.ReadKey();
                return;
            }
            else
            {
                //do nothing    
            }

            Console.WriteLine("Would you like to see your favourite foods rated in order from most to least favourite?. Please type yes or no");
            Console.WriteLine(" "); //break

            var displayfoods = Console.ReadLine();
            switch (displayfoods)
            {
                case "yes":
                    foreach (var item in favfoods.OrderBy(Key => Key.Value))

                        Console.WriteLine($"Your rating for {item.Key} was {item.Value}");
                    Console.WriteLine(" "); //Break
                    break;
                case "no":
                    break;
                default:
                    Console.WriteLine("Invalid input. Food rating will not be displayed");
                    break;
                    // ____________________________________End of Roberts_________________________________________
            }
        }
    }
}
 